# vuecrud

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm json-server db.json
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
